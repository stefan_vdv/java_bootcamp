# Java
In this bootcamp we will write a Java CLI that can manage our todo file. 

## Exercises
1. Create a Java application that reads all todo items from `src/main/resources/todo.csv` 
   Add an extra line that shows how many items are present in the todo list
   
    ```
    Items:
    - Buy groceries
    - Complete Timesheet
    - Finish User Story mht-6
    - Talk to Keshia about demo feedback
    - Write blogpost about TDD
    - Walk the dog
    - Prepare Java Bootcamp
    - Call dentist
    - Read changelog new spring boot version
    - Install new laptop
   
    Nr of items: 10
    ```
   
2. An item can have a priority: A, B, C, ... 
   Change our application so that it first shows the items with priority and in order
   
    ```
    Items:
    - Complete Timesheet
    - Prepare Java Bootcamp
    - Buy groceries
    - Finish User Story mht-6
    - Talk to Keshia about demo feedback
    - Write blogpost about TDD
    - Walk the dog
    - Call dentist
    - Read changelog new spring boot version
    - Install new laptop
   
    Nr of items: 10
    ```

3. An item can have a context, e.g. "optis". Change our application so that it only shows the items 
   that have the context "optis". Also, update the total at the last line.
   
    ```
    Items:
    - Complete Timesheet
    - Prepare Java Bootcamp
    - Talk to Keshia about demo feedback
    - Read changelog new spring boot version 

    Nr of items: 4
    ```

4. We want our user to choose if he/she wants to see a filtered list or not. Change the application so that 
   when it starts we'll ask the users input
  
    ```
    1. Show all items
    2. Show items with context "optis"
   
    Choose action: 2
   
    Items:
    - Complete Timesheet
    - Prepare Java Bootcamp
    - Talk to Keshia about demo feedback
    - Read changelog new spring boot version
   
    Nr of items: 4
    ```

5. Users make mistakes, so it's good to have some validation here. 
   If the user enters something different from 1 or 2, warn the user and re-ask the question
   
    ```
    1. Show all items
    2. Show items with context "optis"
   
    Choose action: 11
   
    There is no menu item 11! 
   
    1. Show all items
    2. Show items with context "optis"
    
    Choose action: 
    ```  
   
6. It's great that we can filter the items with context "optis", but it would be better if 
   the user can choose the context to filter on. Change the application so that the user can get a 
   follow up question to filter on a specific context. If no items where found for a specified context, show 
   a message to the user.
   
    ```
    1. Show all items
    2. Search items by context
       
    Choose action: 2
    Choose context: optis
    ``` 
   
7. Add an option to filter on projects as well.

    ```
    1. Show all items
    2. Search items by context
    3. Search items by project
    ``` 

8. Add an option that allows you to add an item to the list. 
   The item should be saved in `src/main/resources/todo.csv`.

    ```
    1. Show all items
    2. Search items by context
    3. Search items by project
    4. Add item
   
    Choose action: 4
    Item: Finish this awesome bootcamp
    Item added!
    ``` 

9. Add a fifth item that allows you to update an item. When you choose this option, the application shows an id for every 
   item. Based on this id, you can select the item and update it.
   
    ```
    1. Show all items
    2. Search items by context
    3. Search items by project
    4. Add item
    5. Update item
  
    Choose action: 5
    
    Items:
    1. Buy groceries
    2. Complete Timesheet
    3. Finish User Story mht-6
    4. Talk to Keshia about demo feedback
    5. Write blogpost about TDD
    6. Walk the dog
    7. Prepare Java Bootcamp
    8. Call dentist
    9. Read changelog new spring boot version
    10. Install new laptop
   
    Choose item: 6
    New value: buy a dog
    Item updated!
    ``` 

10. In the same way, add a sixth menu item to delete a todo item.

    ```
    1. Show all items
    2. Search items by context
    3. Search items by project
    4. Add item
    5. Update item
    6. Delete item
   
    Choose action: 6
   
    Items:
    1. Buy groceries
    2. Complete Timesheet
    3. Finish User Story mht-6
    4. Talk to Keshia about demo feedback
    5. Write blogpost about TDD
    6. Buy a dog
    7. Prepare Java Bootcamp
    8. Call dentist
    9. Read changelog new spring boot version
    10. Install new laptop
   
    Choose item: 1
    Item deleted!
    ``` 

11. Add a new menu item that allows you to mark an item as done. The line in the file should be saved with an 'x'

    ```
    1. Show all items
    2. Search items by context
    3. Search items by project
    4. Add item
    5. Update item
    6. Delete item
    7. Mark item as done
  
    Choose action: 6
   
    Items:
    1. Complete Timesheet
    2. Finish User Story mht-6
    3. Talk to Keshia about demo feedback
    4. Write blogpost about TDD
    5. Buy a dog
    6. Prepare Java Bootcamp
    7. Call dentist
    8. Read changelog new spring boot version
    9. Install new laptop
   
    Choose item: 9
    Item marked as done!
    ``` 
    
12. When we show items that are done on the screen, the x looks a bit strange. replace the x with a check mark.
  
    ```
    - Call dentist
    - ✓ Read changelog new spring boot version
    ``` 
   
13. Change the application so that the priorities, contexts and projects become case-insensitive.

14. Some items have a creation date. Search the oldest created item and show how many days ago it was created

    ```
    Oldest message: Finish User Story mht-6 (created 97 days ago)
    ```
    
15. We found out that our colleague Andy is using an existing application to manage his todos. He is using: http://todotxt.org/. This application is using a .txt file instead of a .csv file to store the todo items. Change our application so that Andy can use his .txt file in our own application. We give the user the option to choose which file to use.

    ```
    Please select a file to use:
    1. todo.csv
    2. todo.txt

    Choose action: 2

    -- Using todo.txt --

    1. Show all items
    2. Search items by context
    3. Search items by project
    4. Add item
    5. Update item
    6. Delete item
   
    Choose action: 1
   
    Items:
    - Complete Timesheet
    - Prepare Java Bootcamp
    - Buy groceries
    - Finish User Story mht-6
    - Talk to Keshia about demo feedback
    - Write blogpost about TDD
    - Walk the dog
    - Call dentist
    - Read changelog new spring boot version
    - Install new laptop
   
    Nr of items: 10
    ``` 