package be.optis.writer;

import be.optis.model.Todo;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class WriterCSV implements be.optis.writer.Writer {
    private String filePath;
    public WriterCSV(String filePath){
        this.filePath = filePath;
    }

    public void write(List<Todo> todos){
        try(Writer writer = new FileWriter((filePath))){
            writer.append("COMPLETED,PRIORITY,COMPLETION_DATE,CREATION_DATE,DESCRIPTION,PROJECT_TAG,CONTEXT_TAG\n");
            StatefulBeanToCsv<Todo> beanToCsv = new StatefulBeanToCsvBuilder<Todo>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();
            beanToCsv.write(todos);
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e){
            e.printStackTrace();
        }
    }
}
