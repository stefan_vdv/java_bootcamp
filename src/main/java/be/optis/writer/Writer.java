package be.optis.writer;

import be.optis.model.Todo;

import java.util.List;

public interface Writer {
    void write(List<Todo> todos);
}
