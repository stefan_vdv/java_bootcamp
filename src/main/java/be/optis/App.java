package be.optis;

import be.optis.model.Todo;
import be.optis.reader.Reader;
import be.optis.reader.ReaderCSV;
import be.optis.service.TodoService;
import java.io.*;
import java.util.*;

public class App {
    static List<Todo> todos;
    static Scanner in = new Scanner(System.in);
    static String filePath = "src/main/resources/todo.csv";
    static Reader reader = new ReaderCSV(filePath);
    static TodoService todoService = new TodoService(in, filePath);
    public static void main(String[] args) throws IOException {
        Boolean askingInput = true;

        // Get all items
        todos = reader.read();
        // Sorting
        todos = todoService.sortTodos(todos);
        // Ask user input
        while(askingInput){
            System.out.println("1. Show all items " +
                    "\n2. Search items by context" +
                    "\n3. Search items by project" +
                    "\n4. Add item" +
                    "\n5. Update item" +
                    "\n6. Delete item" +
                    "\n7. Mark item as done");
            System.out.print("Your choice: ");

            String choice = in.nextLine().toLowerCase();

            switch (choice){
                case "1": askingInput = false; todoService.findDays(todos); break;
                case "2": askingInput = false; todoService.filterTodos(1, todos); break;
                case "3": askingInput = false; todoService.filterTodos(2, todos); break;
                case "4": askingInput = false; todoService.addTodo(todos); break;
                case "5": askingInput = false; todoService.updateTodo(todos); break;
                case "6": askingInput = false; todoService.deleteTodo(todos); break;
                case "7": askingInput = false; todoService.markTodo(todos); break;
                default:
                    System.out.println("There is no menu choice " + choice);
                    break;
            }
        }



    }
}
