package be.optis.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class Todo {

    @CsvBindByPosition( position = 0)
    @CsvBindByName(column = "COMPLETED")
    private boolean completed;

    @CsvBindByPosition( position = 1)
    @CsvBindByName(column = "PRIORITY")
    private String priority;

    @CsvBindByPosition( position = 2)
    @CsvBindByName(column = "COMPLETION_DATE")
    private String completionDate;

    @CsvBindByPosition( position = 3)
    @CsvBindByName(column = "CREATION_DATE")
    private String creationDate;

    @CsvBindByPosition( position = 4)
    @CsvBindByName(column = "DESCRIPTION")
    private String description;

    @CsvBindByPosition( position = 5)
    @CsvBindByName(column = "PROJECT_TAG")
    private String projectTag;

    @CsvBindByPosition( position = 6)
    @CsvBindByName(column = "CONTEXT_TAG")
    private String contextTag;

    public Todo(){
    }

    public Todo(boolean completed, String priority, String completionDate, String creationDate, String description, String projectTag, String contextTag){
        this.completed = completed;
        this.priority = priority;
        this.completionDate = completionDate;
        this.creationDate = creationDate;
        this.description = description;
        this.projectTag = projectTag;
        this.contextTag = contextTag;
    }


    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectTag() {
        return projectTag;
    }

    public void setProjectTag(String projectTag) {
        this.projectTag = projectTag;
    }

    public String getContextTag() {
        return contextTag;
    }

    public void setContextTag(String contextTag) {
        this.contextTag = contextTag;
    }

//    @Override
//    public String toString() {
//        return completed +
//    }
}
