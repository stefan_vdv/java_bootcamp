package be.optis.service;

import be.optis.model.Todo;
import be.optis.writer.Writer;
import be.optis.writer.WriterCSV;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TodoService {
    private Scanner in;
    private Writer writer;

    public TodoService(Scanner in, String filePath){
        this.in = in;
        this.writer = new WriterCSV(filePath);
    }

    public List<Todo> sortTodos(List<Todo> todos){
        return todos.stream()
                .sorted(Comparator.comparing(Todo::getPriority, Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toList());
    }
    public void filterTodos(int filter, List<Todo> todos){
        if(filter == 1){
            System.out.print("Choose context: ");
            String context = in.nextLine().toLowerCase();
            todos = todos.stream()
                    .filter(e -> Objects.nonNull(e.getContextTag()))
                    .filter(e -> e.getContextTag().equalsIgnoreCase(context)).collect(Collectors.toList());
        }
        else {
            System.out.print("Choose project: ");
            String context = in.nextLine().toLowerCase();
            todos = todos.stream()
                    .filter(e -> Objects.nonNull(e.getProjectTag()))
                    .filter(e -> e.getProjectTag().equalsIgnoreCase(context)).collect(Collectors.toList());
        }
        printItems(todos);
    }

    public void addTodo(List<Todo> todos){
        Todo newTodo = new Todo();
        System.out.print("Item: ");
        String description = in.nextLine().toLowerCase();
        newTodo.setDescription(description);
        todos.add(newTodo);

        writer.write(todos);

        System.out.println("Item added!");
        printItems(todos);
    }

    public void updateTodo(List<Todo> todos){
        System.out.println("Items: ");
        for (int i = 0; i < todos.size(); i++) {
            System.out.println( (i+1) + ". " + todos.get(i).getDescription());
        }
        System.out.print("Choose item: ");
        String chosenNumber = in.nextLine().toLowerCase();
        System.out.print("New value: ");
        String newValue = in.nextLine().toLowerCase();
        todos.get(Integer.parseInt(chosenNumber)-1).setDescription(newValue);
        writer.write(todos);
        System.out.println("Item updated!");
        printItems(todos);
    }

    public void deleteTodo(List<Todo> todos){
        System.out.println("Items: ");
        for (int i = 0; i < todos.size(); i++) {
            System.out.println( (i+1) + ". " + todos.get(i).getDescription());
        }
        System.out.print("Choose item: ");
        String chosenNumber = in.nextLine().toLowerCase();
        todos.remove(Integer.parseInt(chosenNumber)-1);
        writer.write(todos);
        System.out.println("Item Deleted!");
        printItems(todos);
    }

    public void markTodo(List<Todo> todos){
        System.out.println("Items: ");
        for (int i = 0; i < todos.size(); i++) {
            System.out.println( (i+1) + ". " + todos.get(i).getDescription());
        }
        System.out.print("Choose item: ");
        String chosenNumber = in.nextLine().toLowerCase();
        todos.get(Integer.parseInt(chosenNumber)-1).setCompleted(true);
        writer.write(todos);
        System.out.println("Item marked as done!");
        printItems(todos);
    }

    public void findDays(List<Todo> todos){
        String oldestDateString = "2025/12/30";
        int indexOfOldest = 0;
        for (int i = 0; i < todos.size(); i++) {
            String date1 = todos.get(i).getCreationDate();
            if (date1 != null) {
                if (date1.compareTo(oldestDateString) < 0) {
                    oldestDateString = date1;
                    indexOfOldest = i;
                }
            }
        }
        LocalDate dateNow = LocalDate.now();
        LocalDate oldestDate = LocalDate.parse(oldestDateString);
        Long numberOfDays = Duration.between(oldestDate.atStartOfDay(), dateNow.atStartOfDay()).toDays();
        printItems(todos);
        System.out.println("Oldest message: " + todos.get(indexOfOldest).getDescription() + " (created " + numberOfDays +" days ago)");
    }

    public void printItems(List<Todo> todos){
        char checkMark = '\u2713';
        System.out.println("Items: ");
        todos.forEach( e -> System.out.println("- " + (e.isCompleted() ? checkMark : "") + " " + e.getDescription()));
        System.out.println("Nr of items: " + todos.size());
    }
}
