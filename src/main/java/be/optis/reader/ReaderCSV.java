package be.optis.reader;

import be.optis.model.Todo;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class ReaderCSV implements Reader {
    private String filePath;

    public ReaderCSV(String filePath){
        this.filePath = filePath;
    }

    public List<Todo> read() throws FileNotFoundException {
        return new CsvToBeanBuilder<Todo>(new FileReader(filePath))
                .withType(Todo.class)
                .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
                .withSkipLines(1)
                .build().parse();
    }
}
