package be.optis.reader;

import be.optis.model.Todo;

import java.io.FileNotFoundException;
import java.util.List;

public interface Reader {
    List<Todo> read() throws FileNotFoundException;
}
